// ==UserScript==
// @name        betting6: PZ date redirector and navigator
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-betting6-pz-date-redirector-navigator
// @downloadURL https://bitbucket.org/dsjkvf/userscript-betting6-pz-date-redirector-navigator/raw/master/pzdrn.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-betting6-pz-date-redirector-navigator/raw/master/pzdrn.user.js
// @match       *://*.predictz.com/*
// @run-at      document-start
// @version     1.0.1
// @grant       none
// ==/UserScript==


// INIT

var url = window.location.href;
var urlLastPart = url.substr(url.lastIndexOf('/') + 1);
var urlLastLastPart = url.substr(url.lastIndexOf('/', url.lastIndexOf('/') - 1));
var curDate = new Date();


// HELPERS

function redirectToDate(url, delta = 0) {
    if (/^\/\d+\/$/.test(urlLastLastPart)) {
        var parts = urlLastLastPart.split(/[/]+/);
        curDate = new Date(new Date(parts[1].slice(0,4) + '-' + parts[1].slice(4,6) + '-' + parts[1].slice(6,8)).getTime() + delta * 24 * 60 * 60 * 1000)
    }

    var dd = curDate.getDate();
    if (dd < 10) {dd = '0' + dd};
    var mm = curDate.getMonth() + 1; //January is 0!
    if (mm < 10) {mm = '0' + mm};
    var yyyy = curDate.getFullYear();

    var datePage = '' + yyyy + mm + dd;
    return window.location.origin + '/predictions/' + datePage + '/'
}

// MAIN

// Immediately redirect to today if an url doesn't contain a link to a date
if (url == window.location.protocol + '//' + window.location.hostname + '/predictions/' || url == window.location.protocol + '//' + window.location.hostname + '/') {
    window.location.replace(redirectToDate(url));
}

// Add hotkeys to navigate tips pages back an forth
addEventListener('keyup', function(e) {
    if (e.ctrlKey ||
        e.altKey ||
        e.metaKey ||
        e.shiftKey ||
        (e.which !== 74 /*j*/ && e.which !== 75 /*k*/)) {
        return;
    }
    e.preventDefault();

    if (e.which == 74) {
        window.location.replace(redirectToDate(url, -1));
    } else if (e.which == 75) {
        window.location.replace(redirectToDate(url, +1));
    }
})
