Predictz Date Redirector & Navigator
====================================

## About

This is a simple [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting a front page on [Predictz](https://predictz.com/) -- will instantly redirect to the today's tips page, and will also add `j/k` hotkeys to navigate the tips' pages back and forth.
